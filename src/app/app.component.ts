import { Component, Inject, PLATFORM_ID } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { isPlatformBrowser } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  
})
export class AppComponent {
  constructor(@Inject(PLATFORM_ID) private platformId: Object, private translate: TranslateService) {
    if (isPlatformBrowser(this.platformId)) {
      translate.setDefaultLang('en');
      translate.addLangs(['en', 'fr']);
    }
}      
public changeLanguage(lang: string){
  this.translate.use(lang)
}
  
}
