import { AfterViewInit, Component, ElementRef, OnInit  } from '@angular/core';
import { ProductSlider } from '../../../shared/data/slider';
import { Product } from '../../../shared/classes/product';
import { ProductService } from '../../../shared/services/product.service';
declare let Swiper: any;
@Component({
  selector: 'app-fashion-one',
  templateUrl: './fashion-one.component.html',
  styleUrls: ['./fashion-one.component.scss'],

})


export class FashionOneComponent implements OnInit, AfterViewInit {
  
  public products: Product[] = [];
  public productCollections: any[] = [];
  public active:any;
  constructor(private elementRef: ElementRef,
    public productService: ProductService) {
    this.productService.getProducts.subscribe(response => {
      this.products = response.filter(item => item.type == 'fashion');
    
      // Get Product Collection
      this.products.filter((item) => {
        if(item.collection){
          item.collection.filter((collection) => {
            const index = this.productCollections.indexOf(collection);
            if (index === -1) this.productCollections.push(collection);
          })
        }
      })
    });
  }
  ngAfterViewInit(): void {
    const swiperContainer = this.elementRef.nativeElement.querySelector('.swiper-wrapper');

    setTimeout(() => {
      new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 20,
        pagination: '.swiper-pagination',
        paginationClickable: false,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        direction: 'horizontal',
        loop: false,
        updateOnWindowResize: true,
        // breakpointsBase: 'container',
        preloadImages: true,
         preventClicks: true,
        onInit: function(swiper:any) {
          swiperContainer.style.visibility = 'visible'; // Show the swiper container after initialization
        }
      });
    }, 300); // Delay of 500 milliseconds (adjust as needed)
  }

  ngOnDestroy(): void {
  }


  public ProductSliderConfig: any = ProductSlider;

public sliders = [{
    title: 'welcome to fashion',
    subTitle: 'Men fashion',
    image: "assets/data/fashion/manFashion.jpg"
  }, {
    title: 'welcome to fashion',
    subTitle: 'Women fashion',
    image: "assets/data/fashion/womwn.jpg"
  }]

  // Collection banner
  public collections = [{
    image:"assets/data/fashion/man-suit.jpg",
    save: 'save 50%',
    title: 'men'
  }, {
    image: "assets/data/fashion/women.jpg" ,
    save: 'save 50%',
    title: 'women'
  }];

  // Blog
  public blog = [{
    image: 'assets/img/cat1.jpg',
    date: '25 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/img/cat2.jpg',
    date: '26 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, {
    image: 'assets/img/cat3.jpg',
    date: '27 January 2018',
    title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
    by: 'John Dio'
  }, 
  // {
  //   image: 'assets/images/blog/4.jpg',
  //   date: '28 January 2018',
  //   title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
  //   by: 'John Dio'
  // }
];

  // Logo
  public logo = [{
    image: 'assets/images/logos/1.png',
  }, {
    image: 'assets/images/logos/2.png',
  }, {
    image: 'assets/images/logos/3.png',
  }, {
    image: 'assets/images/logos/4.png',
  }, {
    image: 'assets/images/logos/5.png',
  }, {
    image: 'assets/images/logos/6.png',
  }, {
    image: 'assets/images/logos/7.png',
  }, {
    image: 'assets/images/logos/8.png',
  }];

  ngOnInit(): void {
    
  }

  // Product Tab collection
  getCollectionProducts(collection: any) {
    return this.products.filter((item: any) => {
      if(item.collection){
        if (item.collection.find((i:any) => i === collection)) {
          return item
        }
      }
    })
  } 

}
