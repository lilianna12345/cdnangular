import { Component, OnInit, Input } from '@angular/core';
import { BlogSlider } from '../../../shared/data/slider';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  
  @Input() blogs: any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  public BlogSliderConfig: any = BlogSlider;
  // public blog = [{
  //   image: 'assets/images/blog/1.jpg',
  //   date: '25 January 2018',
  //   title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
  //   by: 'John Dio'
  // }, {
  //   image: 'assets/images/blog/2.jpg',
  //   date: '26 January 2018',
  //   title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
  //   by: 'John Dio'
  // }, {
  //   image: 'assets/images/blog/3.jpg',
  //   date: '27 January 2018',
  //   title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
  //   by: 'John Dio'
  // }, {
  //   image: 'assets/images/blog/4.jpg',
  //   date: '28 January 2018',
  //   title: 'Lorem ipsum dolor sit consectetur adipiscing elit,',
  //   by: 'John Dio'
  // }];

}
