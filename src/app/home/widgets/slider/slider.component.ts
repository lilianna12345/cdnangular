import { Component, OnInit, Input } from '@angular/core';
import { HomeSlider } from '../../../shared/data/slider';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  
  @Input() sliders: any[] = [];
  @Input() class !: string;
  @Input() textClass !: string;
  @Input() category !: string;
  @Input() buttonText !: string;
  @Input() buttonClass !: string;
 
  constructor() { }

  ngOnInit(): void {
    
  }
  public slider = [{
    title: 'welcome to fashion',
    subTitle: 'Men fashion',
    image: "assets/data/fashion/man.jpg"
  }, {
    title: 'welcome to fashion',
    subTitle: 'Women fashion',
    image: "assets/data/fashion/2024f.jpg"
  }]
  public HomeSliderConfig: any = HomeSlider;

}
