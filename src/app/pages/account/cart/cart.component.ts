import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../shared/services/product.service';
import { Product } from '../../../shared/classes/product';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public products=[{
    images: 'assets/img/cat1.jpg',
    price: '150',
    title: 'Trim Dress,',
    quantity: 1
  }, {
    images: 'assets/img/cat2.jpg',
    price: '100',
    title: 'Belted Dress',
    by: 1
  }, {
    images: 'assets/img/cat3.jpg',
    price: '112',
    title: 'Fitted Dress',
    quantity: 1
  }
]
  
  constructor(public productService: ProductService) {
    this.productService.cartItems.subscribe((response:any) => this.products = response);
  }

  ngOnInit(): void {
  }
  public get getTotal(): Observable<number> {
    return this.productService.cartTotalAmount();
  }

  // Increament
  increment(product:any, qty = 1) {
    this.productService.updateCartQuantity(product, qty);
  }

  // Decrement
  decrement(product:any, qty = -1) {
    this.productService.updateCartQuantity(product, qty);
  }

  public removeItem(product: any) {
    this.productService.removeCartItem(product);
  }
}
