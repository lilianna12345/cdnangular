import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletonProductBoxComponent } from './skeleton-product-box.component';

describe('SkeletonProductBoxComponent', () => {
  let component: SkeletonProductBoxComponent;
  let fixture: ComponentFixture<SkeletonProductBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SkeletonProductBoxComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SkeletonProductBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
