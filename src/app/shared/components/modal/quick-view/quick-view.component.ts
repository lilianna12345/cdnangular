import { Component, OnInit, OnDestroy, ViewChild, TemplateRef, Input, PLATFORM_ID, Inject, ElementRef } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';
import {  Product } from "../../../classes/product";
import { ProductService } from '../../../../shared/services/product.service';

@Component({
  selector: 'app-quick-view',
  templateUrl: './quick-view.component.html',
  styleUrls: ['./quick-view.component.scss']
})
export class QuickViewComponent implements OnInit, OnDestroy {

  @Input() product!: any;
  @Input() p!: any;
  @Input() currency!: any;
  @ViewChild("quickView", { static: false }) QuickView!: TemplateRef<any>;

  public products!: Product;
  public ImageSrc: string ='';
  public counter: number = 1;
  public modalOpen: boolean = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
              private router: Router,
              public productService: ProductService) { }

  ngOnInit(): void {   
    
  }

   openModal() {
     this.products= this.product;
    this.modalOpen = true;
    if (isPlatformBrowser(this.platformId)) { // For SSR
      const modal = document.getElementById(this.p)  ;
      console.log(modal);
      
      if (modal) {
        modal.classList.add('show');
        modal.style.display = 'block';
        modal.style.backgroundColor= 'rgba(0, 0, 0, 0.5)';
        document.body.appendChild(modal);
        document.body.classList.add('modal-open');
     
      }
    }
  }

  closeModal() {
    this.modalOpen = false;
    if (isPlatformBrowser(this.platformId)) { // For SSR
      const modal = document.getElementById(this.p);
      if (modal) {
        modal.classList.remove('show');
        modal.style.display = 'none';
        modal.remove();
        document.body.classList.remove('modal-open');
      }
    }
  }

  // Get Product Color
  Color(variants:any) {
    const uniqColor = [];
    for (let i = 0; i < Object.keys(variants).length; i++) {
      if (uniqColor.indexOf(variants[i].color) === -1 && variants[i].color) {
        uniqColor.push(variants[i].color);
      }
    }
    return uniqColor;
  }

  // Get Product Size
  Size(variants:any) {
    const uniqSize = [];
    for (let i = 0; i < Object.keys(variants).length; i++) {
      if (uniqSize.indexOf(variants[i].size) === -1 && variants[i].size) {
        uniqSize.push(variants[i].size);
      }
    }
    return uniqSize;
  }

  // Change Variants
  ChangeVariants(color:any, product:Product) {
    product.variants?.forEach((item:any) => {
      if (item.color === color) {
        product.images?.forEach((img:any) => {
          if (img.image_id === item.image_id) {
            this.ImageSrc = img.src;
          }
        });
      }
    });
  }

  // Increment
  increment() {
    this.counter++;
  }

  // Decrement
  decrement() {
    if (this.counter > 1) this.counter--;
  }

  // Add to cart
  async addToCart(product: any) {
    product.quantity = this.counter || 1;
    const status = await this.productService.addToCart(product);
    if (status) this.router.navigate(['/shop/cart']);
  }

  ngOnDestroy() {
    if (this.modalOpen) {
      this.closeModal();
    }
  }
 get getProductStock(){
  return Number(this.product.stock)
 }
}
