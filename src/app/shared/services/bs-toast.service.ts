import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })

  export class ToastService {
    private toastSubject = new Subject<string>();

    constructor() { }

    showToast(messege: string){
        this.toastSubject.next(messege);
    }

    getToastSubject() {
        return this.toastSubject.asObservable();
      }
  }